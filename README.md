# Leetcode

Collection of my Leetcode solutions

## Things to notice
| Problems        | Solutions                                                     |
|-----------------|---------------------------------------------------------------|
| lastStoneWeight | Sort first instead of searching for highest stones. Insert the weight to the sorted stones list (keep it sorted). It also possible to use the recursive function (but the sort will be execute many times) |
| lengthOfLongestSubstring | Use hash map to store the character (key) and its index (value). Use on variable for determine the maximum length and another pointer to determine the index from where the substring start                                                               |
| findMedianSortedArrays| Don't merge the two arrays (and sort) because the performance should be O(log(m+n)). Whenever we see the O(log(m)) it makes us thing about the binary search. Also, think about dividing the inputs into LEFT and RIGHT where LEFT is the part of smaller numbers. We only need to list through half of the input numbers to calculate the median |
| longestPalindrome| Solve the problem of odd and even for center of Palindrome |
| clonedGraph| For a graph, 3 things to consider: Recursive, visited Node, empty Node (return node) |
| reverseOnlyLetters| There are many possible solutions to solve the problem, but the common ways is to use two pointers (start, end) and swap if the character in those pointers are both English alpha. Noted that you have to change the string to list for fast swap in Python |
| spiralMatrix| The solution is to follow: first row, last elements, last row, first elements. Always consider testing for the empty list |
| breakPalindrome| The solution is to check from 0 to max string length. If any of the character is not 'a' AND there is another not 'a' character in the next characters, change the first not 'a' into 'a' (smallest value). If the previous condition is not met, it is consider that the change of a not 'a' will resulted in all 'a' string which is Palindrome. In that case, change the last character to 'b'|