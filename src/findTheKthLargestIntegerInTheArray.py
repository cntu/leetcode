class Solution:
    def kthLargestNumber(self, nums: List[str], k: int) -> str:
        ### Possible Solution
        # Sort the integer
        # Find the n^th largest number in the array
        # Return the integer as string

        # Problem: You have to convert all the string array to integer O(n)
        # Sort the integer array and return the n^th number
        intList = []
        for numStr in nums:
            intList.append(int(numStr))
        # Sort the intList
        return str(sorted(intList)[-k])

        # Enhance: How about checking while converting, iterating through the number
        # By that, we can save more time for the sort

        # Enhance 2nd: length of each string in the array
        #             + The string with greater number of characters represent a higher number

