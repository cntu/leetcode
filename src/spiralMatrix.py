class Solution:
    # Firstly, recursive is possible (or while)
    # The spiral for each round will have the following steps:
    #   1. Get elements of the first array ([1,2,3], [4,5,6], [7,8,9]) ---> [1,2,3] -> input[0]
    #   2. Get the last elements of other arrays (except the first and last)
    #   3. Get the reverse order of the last array
    #   4. Get the first elements of other arrays (except the first and last)
    #   5. Remove all the extracted elements to get the sub-matrix

    #   6. Input the sub-matrix to step 1
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        # We will update the inputted matrix for each spiral round
        output = []
        while(len(matrix) > 0):
            output += matrix.pop(0)
            # Single element
            if len(matrix) < 1:
                break
            # Only last element
            if len(matrix) == 1:
                # Get last element and reverse
                output += matrix.pop()[::-1]
                break
            # Get the last elements for other array
            first_value = []
            for index in range(0,len(matrix) - 1):
                # Assuming that the matrix has more than 1 element
                # Is there any possibility that the matrix[index] empty ? (Assume that it is not occur)
                # Remove the last element from each matrix
                last_value = matrix[index]
                if last_value:
                    output.append(last_value.pop())
                if last_value:
                    first_value.append(last_value.pop(0))
            last_row_r = matrix.pop()[::-1]
            output += last_row_r
            output += first_value[::-1]
        return output


