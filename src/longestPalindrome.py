class Solution:
    def longestPalindrome(self, s: str) -> str:
        # palindromic: a string that return same output when count forward and backwards
        # Problems:
        #   + The start and end of palindromic (2 pointers)

        # "abbaddae" -> stack = [(a,b,b,{a),d,d,a},e] -> output: abba or adda
        # Notice that we only need to find one

        # A fast solution is to iterator through all char and check whether they
        # are the center point of an palindromic
        # There are two cases:
        #           "aba" --> b is a center point, in that case we already has the previous stack values [a]. We can check if the next character is also matched [a]
        #           "abba" --> b is around center point. Although the previous values does not matched the next values, the next value is actually match current value [b]
        # If those too cases do not meet, then we add b to the stack

        # Things to be careful:
        #     - Conflict between odd and even of the palindromic

        max_pal = 0
        pal = ""
        last_pal = ""
        for index, value in enumerate(s):    # O(n)
            next_index = index + 1
            if index == 0:    # The first value, add to pal and stack
                pal += value
                continue
            if index == len(s) - 1:    # end of string
                next_index = index
            # Check if index is around center point
            if next_index > index and s[next_index] == s[index - 1]:
                pal = self.getPalindromic(s[:index], s[next_index:])
                pal = pal[::-1] + value + pal
                if max_pal < len(pal):
                    last_pal = pal
                    max_pal = len(pal)
            if value == s[index - 1]:
                # Trigger palindromic
                pal = self.getPalindromic(s[:index], s[index:])
                pal = pal[::-1] + pal
                if max_pal < len(pal):
                    last_pal = pal
                    max_pal = len(pal)
            else:
                pal = ""  # reset pal

        if not last_pal:
            return s[0]
        return last_pal

    def getPalindromic(self, stack: str, check_str: str) -> list:
        pal = ""
        for index, char in enumerate(check_str):
            if index > len(stack) - 1:
                break
            if stack[-(index+1)] == char:
                pal += char
                continue
            break
        return pal