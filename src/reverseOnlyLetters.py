class Solution:
    def reverseOnlyLetters(self, s: str) -> str:
        # One of the solution for this is to use two pointers
        # One from the start of string
        # Another from the end of string
        # Change the order of start-end pointer if they are both letters (65 - 90 and 97 - 122)
        #
        # Example:
        #  [ab-1cd] --> [dc-1ba]
        start, end = 0, len(s) - 1
        list_s = list(s)
        while(start < end):
            if not s[start].isalpha():
                start += 1
            elif not s[end].isalpha():
                end -= 1
            else:
                # Switch
                list_s[start], list_s[end] = list_s[end], list_s[start]
                # Move the pointers
                start += 1
                end -= 1
        return ''.join(list_s)

    # or using isalpha() library
    #def isLetter(self, char):
    #    # Check with the ord() function
    #    if ord(char) < 65 or ord(char) > 122:
    #        return False
    #    # Check if char between 65 and 90
    #    if ord(char) < 91:
    #        return True
    #    # Check if char between 97 and 122
    #    if ord(char) > 96:
    #        return True
    #    # Other
    #    return False