"""
# Definition for a Node.
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []
"""
class Solution:
    # Normally for a graph problem, we have two issues:
    #       + Visited
    #       + Ended node
    # and we things about: RECURSIVE 
    def cloneGraph(self, node: 'Node') -> 'Node':
        return self._clone(node, {})

    def _clone(self, node: 'Node', visited: 'Dict') -> 'Node':
        if not node: 
            return node
        if visited.get(node.val) is not None:
            # Return node in the visited
            return visited.get(node.val)
        # Make new node 
        cloned = Node(node.val, None)
        # Add to the visited 
        visited[cloned.val] = cloned
        # loop might occurs because the cloned might be inside
            # one of the neighbor as its neighbor
        for neighbor in node.neighbors:
            cloned.neighbors.append(self._clone(neighbor, visited))
        return cloned      