class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        # If merged array is odd, then the median is middle point
        # If not, then the median is average of middle points
        # Example:
        #    1. [1,6] and [2,4] --> [1,2,4,6] -> median is 3
        #    2. [1,2,3] and [7,10] -> [1,2,3,7,10] -> median is 3

        # Problem here is: O(log (m+n)) requirement. In that case,
        # if we merge the two array and sort again, the requirment is not satisfy

        # We know that in order to get the log(n), we have to do the
        # binary search. That's means we must find the result by using
        # only half of the provided data. For that, we must find the median
        # by using only half of each input list. For example 1., we have 4 values,
        # then we need only know where is the half of those to find medium for example         # [1,2 | <- this point ]

        # How to do that ? For each of the input, we consider there is 2 parts:
        # LEFT and RIGHT partitions.

        # In the example 1. If we assumpt the partition point of [1,6] is from the middle [1 | 6], then we only need 1 left for the 2nd input, which resulted in [2|4]. In that case, we could see that the partitions are correct (LEFT is 1,2 and RIGHT is 4, 6). The median will be the (max(LEFT) and min(RIGHT)) / 2.

        # For 2nd example: [1,2,3] and [7,10]. The total is 5. Then we only need 5//2 = 2. Consider the shorter array which is [7 | 10] then we only need 1 for the larger list: [1 | 2,3]. This output is not correct since LEFT = 1,7 where 7 is actually larger than some of the RIGHT values which are 2,3.

        # The important condition here should be: The \values of the LEFT should smaller than all the RIGHT values of the inputs.

        # Return back to the 2nd example, in that case we need to move the partition of the 2nd list to the left until negative infinity (to get more values from the 1st list). And then we get 1st list: [1,2 | 3] and the 2nd list [7,10]. And the final is [1,2 | 3,7,10]. For that, the median is the min(RIGHT).

        # How to determine the partition point ? We use two pointers Left and Right that used for calculate the median in a list. The pointers point to a smaller list.

        # The code
        # Determine the smaller list is nums1
        if len(nums1) > len(nums2):
            nums1, nums2 = nums2, nums1
        total = len(nums1) + len(nums2)
        half = total // 2
        left, right = 0, len(nums1) - 1
        while True:
            part1 = (left + right) // 2
            part2 = half - part1 - 2   # Because we start the index with 0

            left1 = nums1[part1] if part1 >= 0 else float("-infinity")
            right1 = nums1[part1 + 1] if part1 + 1 < len(nums1) else float("infinity")
            left2 = nums2[part2] if part2 >= 0 else float("-infinity")
            right2 = nums2[part2 + 1] if part2 + 1 < len(nums2) else float("infinity")

            if left1 <= right2 and left2 <= right1:
                # Correct partition, calculate median
                if total % 2:
                    return float(min(right1, right2))
                else:
                    return float((max(left1, left2) + min(right1, right2)) / 2)
            elif left1 > right2:    # some of the left value should be on the right
                right = part1 - 1
            else:
                left = part1 + 1