// 1. Assuming that the input is always palindrome string
// 2. The size of the string can be empty or 1 ( impossible to replace)
import (
	"fmt"
	"strings"
)

func breakPalindrome(palindrome string) string {
	inputLen := len(palindrome)
	if inputLen < 2 {
		return ""
	}
	// Change the values that is not a
	changed := 0
	original := palindrome
	for index := 0; index < inputLen; index++ {
		if string(palindrome[index]) != "a" {
			if changed == 0 {
				palindrome = replaceAtIndex(palindrome, 'a', index)
				changed = 1
			} else {
				// Meet an non-a character
				return palindrome
			}
		}
	}
	// all "a"
	return replaceAtIndex(original, 'b', inputLen-1)
}

func replaceAtIndex(in string, r rune, i int) string {
	out := []rune(in)
	out[i] = r
	return string(out)
}